This module utilizes the ajaxit plugin to convert a Drupal site into an ajax site. 

To use this module:
1. 	Download the ajaxit plugin from https://github.com/sDaniel/ajaxit
2. 	Download the jquery history plugin from http://tkyk.github.com/jquery-history-plugin/
3. 	Place the plugins in sites/all/libraries/ajaxit respectively sites/all/libraries/history
	The main pulgin files should be located in:
	sites/all/libraries/ajaxit/jquery.ajaxit.js
	sites/all/libraries/history/jquery.history.js
3.	Enable the module
4.	Go to settings (admin/settings/ajaxit) and specify the jquery wrapper and other javascript 
	callback functions (check documentation below)

For the plugin documentation, visit http://www.o-minds.com/products/ajaxit